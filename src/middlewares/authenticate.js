import jwt from 'jsonwebtoken';
import User from '../models/user.model';
import HttpStatus from 'http-status';

export default (req, res, next) => {
    const token = req.headers.authorization;

    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                res.status(HttpStatus.UNAUTHORIZED).json('unauthorized');
            } else {
                User.findOne({email: decoded.email}).then(user => {
                    req.currentUser = user;
                    next();
                });
            }
        })
    } else {
        res.status(HttpStatus.UNAUTHORIZED).json('unauthorized');
    }
}