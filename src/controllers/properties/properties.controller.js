import HttpStatus from 'http-status';
import Property from '../../models/property.model';

export const getProperties = (req, res, next) => {
    Property.find({})
        .then(properties => {
            res.status(HttpStatus.OK).json(properties);
        })
        .catch(err => res.status(HttpStatus.BAD_REQUEST).json(err));
};

export const createProperty = (req, res, next) => {
    const reqData = req.body;
    const property = new Property({
        name: reqData.name,
        title: reqData.title,
        parentId: reqData.parentId,
        categoryId: reqData.categoryId,
        userId: req.currentUser._id
    });
    property.save()
        .then(result => {
            res.status(HttpStatus.OK).json(result);
        })
        .catch(err => res.status(HttpStatus.BAD_REQUEST).json(err));
};

export const updateProperty = (req, res, next) => {
    const reqData = req.body;
    Property.findOneAndUpdate(
        {_id: reqData.id},
        {
            $set: {
                name: reqData.name,
                title: reqData.title,
                parentId: reqData.parentId,
                categoryId: reqData.categoryId,
                userId: req.currentUser._id
            }
        },
        {upsert: true}
    )
        .then(result => {
            res.status(HttpStatus.OK).json(result);
        })
        .catch(err => res.status(HttpStatus.BAD_REQUEST).json(err));
};

export const deleteProperty = (req, res, next) => {
    Property.findOneAndDelete({_id: req.params.id})
        .then(result => res.status(HttpStatus.OK).json(result))
        .catch(err => res.status(HttpStatus.BAD_REQUEST).json(err));
};